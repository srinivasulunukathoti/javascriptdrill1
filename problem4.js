// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the 
// dealer data containing only the car years and log the result in the console as it was returned.

function findAllCarsYears(data) {
    //checking the data properly given correct or not
    if (Array.isArray(data)) {
        //To store the years
        const car_years = [];
        for (let index = 0; index < data.length; index++) {
            // push the years into car_years
            car_years.push(data[index].car_year);
        }
        return car_years.toString();
    } else {
        return "sufficient data";
    }
}
// Exports the code
module.exports = { findAllCarsYears };