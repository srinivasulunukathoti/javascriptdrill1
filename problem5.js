// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the 
// previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function findCarsBefore2000(data , year) {
    //checking the data properly given correct or not
    if (Array.isArray(data)) {
        let count = 0;
        for (let index = 0; index < data.length; index++) {
            
            if (data[index].car_year < year) {
               count++;
            }
        }
        return "how many cars were made before the year 2000"+ count;
    } else {
        return "in sufficient"
    }
}
// Exports the code
module.exports = { findCarsBefore2000 };