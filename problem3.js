// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model 
// names into alphabetical order and log the results in the console as it was returned.


function arrangeCarModelsInAlphabetical(data) {
    //checking the data properly given correct or not
    if (Array.isArray(data)) {
        let car_models = [];
        for (let index = 0; index < data.length; index++) {
           for (let index1 = index + 1; index1 < data.length; index1++) {
            if (data[index].car_model > data[index1].car_model) {
                let temp =data[index].car_model;
                data[index].car_model = data[index1].car_model;
                data[index1].car_model=temp;
            }
            
           }
           car_models.push(data[index].car_model);
        }
        return car_models.toLocaleString();
    } else {
        return [];
    }
}
// Exports the code
module.exports = {arrangeCarModelsInAlphabetical};