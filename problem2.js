// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of 
// the last car in the inventory is?  Log the make and model into the console in the format of:

function findLastCarInventory(data) {
    //checking the data properly given correct or not
    if (Array.isArray(data)) {
        const car = data[data.length-1];
        if(car)
        {
            return "Last car is a "+car.id +" car make goes here "+car.car_make +" car model goes here "+car.car_model;
        }
    } else {
        return null;
    }
}
// Exports the code
module.exports = {findLastCarInventory};