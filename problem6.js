function findBMWAndAUDICars(data) {
    //check the data properly given correct or not
    if (Array.isArray(data)) {
        const matchingcars =[];
        for (let index = 0; index < data.length; index++) {
           let carName = data[index].car_make;
           //check the BMW and Audi cars by using the data
           if (carName === "BMW" || carName === "Audi") {
            const carsInformation = JSON.stringify(data[index]);
            matchingcars.push(carsInformation);
           }
        }
        return matchingcars;
    } else {
        return "so";
    }
}
// Exports the code
module.exports = {findBMWAndAUDICars };