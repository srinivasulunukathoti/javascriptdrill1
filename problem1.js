    //The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling 
    // a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of
    function findCarWithId(data,id)
    {
        //check the data properly given correct or not
        if (Array.isArray(data) && typeof id == 'number') {
            for (let index = 0; index < data.length; index++) {
                const car = data[index];
                //checking the id is matching or not
                if (car.id === id) {
                    return car;
                }
                
            }
            return null;
        } else {
            return [];
        }
    };
    // Exports the code
    module.exports ={findCarWithId};